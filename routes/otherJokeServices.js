var controller = require("../controllers/otherServiceController");

var getLegitServices = function(services) {
    for (let i = 0; i < services.length; i++) {
        let serv = services[i];
        console.log(serv.name);
        if (serv.name == undefined || serv.name == "" || serv.address == undefined || serv.name == "JokeService") {

            services.splice(i, 1); 
            i--;
            console.log("REMOVED: " + serv.name + " " + serv.address + " " + i);
        }
    }
    return services;
}

module.exports = function(express) {
    var router = express.Router();
    let allServices = [];
    router.route('/otherJokeServices')
        .get(function (req, res) {
            // Get all the services
            controller.getOtherServices()
                .then(function(val) {
                    allServices = getLegitServices(val);
                    let jokesFromAService = [];
                    // Get all jokes from each service
                    for (let i = 0; i < allServices.length; i++) {
                        let address = allServices[i].address;
                        jokesFromAService.push(controller.getJokesFromService(address));
                    }
                    Promise.all(jokesFromAService).then(function(values) {
                        for (let i = 0; i < values.length; i++) {
                            //If the value isn't the JSON formatted jokes,
                            //make sure the jokeservice gets an empty
                            //array instead
                            if (typeof values[i] == "object") {
                                allServices[i].jokes = values[i];    
                            }
                            else  {
                                allServices[i].jokes = [];
                            }
                        }
                    }).then(function(data) {
                        console.log(data);
                        res.send(allServices);
                    }).catch(function(err) {
                        console.log("Nested CATCH?");
                        console.log(err);
                    });
                })
            .catch(function(err) {
                console.log(err);
                res.status(500).send(err);
            });
        })
    return router;
};
