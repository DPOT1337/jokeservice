var controller = require("../controllers/controller");
module.exports = function(express) {
    var router = express.Router();
    router.route('/api/jokes/:id')
        .get(function (req, res) {
            controller.getJoke(req.params.id).
                then(function(val) {
                    res.json(val);
                })
            .catch(function(err) {
                console.log(err);
                res.status(500).send(err);
            });
        });
    return router;
}
