var controller = require("../controllers/controller");

module.exports = function(express) {
    var router = express.Router();
    router.route('/api/jokes')
        .get(function (req, res) {
            controller.getJokes()
                .then(function(val) {
                    res.json(val);
                })
                .catch(function(err) {
                    console.log(err);
                    res.status(500).send(err);
                });
        })
        .post(function(req, res) {
            controller.createJoke(req.body.setup, req.body.punchline)
                .then(function() {
                    res.json({message: 'Joke saved!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });
    return router;
};
