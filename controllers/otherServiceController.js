"use strict"

var rp = require('request');
var rpp = require('request-promise');

exports.getOtherServices = function () {
    return rpp({
        "method":"GET",
        "uri":"https://krdo-joke-registry.herokuapp.com/api/services",
        "json":true
    });
}

exports.getJokesFromService = function (inAddress) {
    if (inAddress.charAt(inAddress.length - 1) == "/") {
        inAddress = inAddress.substring(0, inAddress.length - 1);
    }
    return new Promise(function (resolve, reject) {
        rp({
            "method":"GET",
            "uri":inAddress + "/api/jokes",
            "json":true
        }, function(error, res, body) {
            if (error || res.statusCode != 200) {
                return resolve([]);
            }
            else {
                return resolve(body);
            }

        });
    });
}
