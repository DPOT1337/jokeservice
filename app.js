"use strict";

// INITIALIZATION
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config');
var rp = require('request-promise');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static('public'));

// MONGODB & MONGOOSE SETUP
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoDBhost);

// Connect to JokeRegistry
var jokeRegistryReq = {
        method: "POST",
        uri: "https://krdo-joke-registry.herokuapp.com/api/services",
        json: true,
        body: {
            "name": "JokeService",
            "address": "https://jokeservicedpo.herokuapp.com/",
            "secret": "jokehahasjov"
        }
};

console.log("Sending a POST request to jokeregistry...");
rp(jokeRegistryReq).then(function(data){
    console.log(data); 
}).catch(function(err) {
    console.log("Error: Could not POST to jokeRegistry");
    console.log(err);
});

var jokesRouter = require('./routes/jokes')(express);
app.use(jokesRouter);

var otherServicesRouter = require('./routes/otherJokeServices')(express);
app.use(otherServicesRouter);

var oneJokeRouter = require('./routes/oneJoke')(express);
app.use(oneJokeRouter);

// START THE SERVER
var port = process.env.PORT || config.defaultPort;
app.listen(port)
console.log('Listening on port ' + port + ' ...');
