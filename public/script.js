$(function () {
    function updateOwnJokes() {
        $('#ownJokes').empty();
        $('input').val("");
        $.get('jokes.hbs')
            .then(function (template) {
                var compiled = Handlebars.compile(template);
                $.getJSON('/api/jokes')
                    .then(function (data) {
                        data.forEach(function (element) {
                            var html = compiled({
                                id: element._id,
                                setup: element.setup,
                                punchline: element.punchline,
                                dataParent: "ownJokes" // Used for collapse
                            });
                            $('#ownJokes').append(html);

                        });
                    })
            });
    }

    function updateOtherJokes() {
        var jokesCompiled, servicesCompiled;
        $('#otherJokes').empty();
        $.get('jokes.hbs').then(function(template) {
            jokesCompiled = Handlebars.compile(template);
            $.get('services.hbs').then(function(template) {
                servicesCompiled = Handlebars.compile(template);
                $.getJSON('/otherJokeServices')
                    .then(function(data){
                        let jokeDiv = $('#otherJokes');
                        data.forEach(function(element) {
                            var html = servicesCompiled({
                                id: element._id,
                                name: element.name,
                                address: element.address,
                            });
                            jokeDiv.append(html);
                            element.jokes.forEach(function (joke) {
                                    html = jokesCompiled({
                                        id: joke._id,
                                        setup: joke.setup,
                                        punchline: joke.punchline,
                                        dataParent: "serviceJoke" + element._id
                                    });
                                    let slctor = "#serviceJoke" + element._id;
                                    $(slctor).append(html);
                            });
                        });
                    });
            });
        });
    }

    function updateAll() {
        updateOwnJokes();
        updateOtherJokes();
    }

    updateAll();

    $('#submitJoke').click(function () {
        var msg = {
            setup: $('#jokeSetup').val(),
            punchline: $('#jokePunchline').val()
        };
        $.post('/api/jokes', msg)
            .then(function (data) {
                updateOwnJokes();
            });
    });
});

